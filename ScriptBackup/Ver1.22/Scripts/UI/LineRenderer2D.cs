﻿using UnityEngine;
using System.Collections;

public class LineRenderer2D : MonoBehaviour {



	//
	//Calculates all values and adjusts the object to the correct transform
	//
	public void render(Vector3 startLocation, Vector3 endLocation, float thickness) {
		//Reset rotation of trail object to 0,0,0
		transform.Rotate(-transform.rotation.eulerAngles);

		//Get offset vector
		Vector3 offset = endLocation - startLocation;
		Vector3 offset2 = new Vector2(offset.x, offset.y);

		//Find the angle of the offset vector in comparison to the x-axis
		float angle = Mathf.Atan2(offset2.y, offset2.x);
		angle = angle * 180f/Mathf.PI;
		
		//Move the trail object to the correct location
		transform.position = startLocation;
		transform.Translate(offset/2);
		
		//Rotate the trail object to the correct rotation
		transform.Rotate(Vector3.forward * angle, Space.Self);
		
		//Find and set the correct scale
		float magnitude = Mathf.Sqrt(Vector2.Dot(offset2, offset2));
		transform.localScale = new Vector3(magnitude, thickness, thickness);
	}


	//
	//Adjusts the object to the correct transform using precalculated values provided
	//
	public void render(Vector3 startLocation, Vector3 offset, float angle, float magnitude, float thickness) {
		//Reset rotation of trail object to 0,0,0
		transform.Rotate(-transform.rotation.eulerAngles);

		//Move the trail object to the correct location
		transform.position = startLocation;
		transform.Translate(offset/2);

		//Rotate the trail object to the correct rotation
		transform.Rotate(Vector3.forward * angle, Space.Self);

		//Set the correct scale
		transform.localScale = new Vector3(magnitude, thickness, thickness);
	}

}
