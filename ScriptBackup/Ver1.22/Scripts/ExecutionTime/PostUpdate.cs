﻿using UnityEngine;
using System.Collections;

//
//Guarenteed to run last every frame, put code that must be run postUpdate here
//
public class PostUpdate : MonoBehaviour {



	//
	// Last function to be called during start cycle when game loads
	//
	void Start () {
	
	}



	//
	// Last function to be called in each frame
	//
	void Update () {

		//Prepare Arrows input for next frame
		ArrowsInput.prepForFrame();
	}
}
