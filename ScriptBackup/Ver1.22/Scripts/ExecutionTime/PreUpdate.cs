﻿using UnityEngine;
using System.Collections;

//
//Guarenteed to run first every frame, put code that must be run preUpdate here
//
public class PreUpdate : MonoBehaviour {
	


	//
	//Very first function that is called when game loads
	//
	void Awake () {
		//Initialize the array that will store input status of arrows
		ArrowsInput.arrowInfo = new bool[24];
	}
	


	//
	//This function is called first every frame
	//
	void Update () {
		Mouse.mouse.UpdateMouse();
	}
}
