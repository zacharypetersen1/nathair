﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Levels : MonoBehaviour {


	private static GameObject apple;
	public Texture2D[] worldOne;			//Each texture represents a level
	public static Levels thisScript;		//Static Reference to this script (necisary for static functions)
	public static int tileCountW;			//Static variables to store number of tiles wide and high
	public static int tileCountH;
	public static float borderW;
	public static float borderH;
	public static float curTileSize;   		//Size of the square tiles
	public static Vector3 originTileLoc;	//Location of the origin tile, i.e. the 0,0 tile
	static Window gameWindow;
	private static int[,] collisionGrid;		//Records which spaces are filled by snake and which are filled by coll. obj etc
											// 0 = open space   1 = snake   2 = wall   3 = apple
	public static int winCount;				//Records how many spaces need to be filled by snake body segments to win


	//
	//Store this script in a static variable
	//
	public void Awake() {
		thisScript = GameObject.Find("Levels").GetComponent<Levels>();
	}



	//
	//Loads a level from levelData
	//
	//Preconditions: Top of State Stack is Game State (i.e. current state is game state)
	//
	public static void loadLevel(int worldNumber, int levelNumber) {

		//Calculate unit size of game window
		gameWindow = new Window();
		gameWindow.setWidth(Layout.unitWidth * (2.0f / 3.0f));
		gameWindow.setHeight(Layout.unitHeight);
		//find the origin location of the game window, assume that it is right-up justified
		gameWindow.setOrigin(new Vector3(Layout.unitWidth - gameWindow.getWidth(), 0, 0));

		//Initialize the Analog stick window
		Window analogWindow = new Window();
		analogWindow.setWidth(Layout.unitWidth - gameWindow.getWidth());
		analogWindow.setHeight(Layout.unitHeight);
		analogWindow.setOrigin( Vector3.zero );
		analogWindow.stretchPatternToWindow("AWPattern", "AWPattern-edge", "AWPattern-corner", 4, -1, 0);

		//
		//Get basic info from the level data
		//
		tileCountW = thisScript.worldOne[levelNumber].width;
		tileCountH = thisScript.worldOne[levelNumber].height;
		winCount = tileCountH * tileCountW;
		collisionGrid = new int[tileCountW, tileCountH];

		//
		//Calculate best fit sizes for width and height
		//	Controll column (left bounded) 20% of total space
		//	Game window 80% x-space (right justified
		//		*border = 10% of remaining size
		//		*tileBorder = 10% of area allocated for tile
		//		*the rest (80%) goes to the tile
		//

		//TODO add visualization of game window here

		//Set up the game window
		borderW = gameWindow.getWidth() * 0.03f;
		borderH = gameWindow.getHeight() * 0.03f;
		float tempTileW = (gameWindow.getWidth() - 2*borderW) / tileCountW;
		float tempTileH = (gameWindow.getHeight() - 2*borderH) / tileCountH;

		//
		//Evaluate which (between height and width) is tighter fit, then adjust other to match
		//
		if(tempTileH < tempTileW) {
			curTileSize = tempTileH;
			borderW = ( gameWindow.getWidth() - (curTileSize * tileCountW ) ) / 2;
		}
		else {
			curTileSize = tempTileW;
			borderH = ( gameWindow.getHeight() - (curTileSize * tileCountH ) ) / 2;
		}

		//
		//Loop through and create the background tiles
		//
		for(int w = 0; w < tileCountW; w++) {
			for(int h = 0; h < tileCountH; h++) {
				Color32 tempColor = (Color32)thisScript.worldOne[levelNumber].GetPixel(w, h);
				GameObject tile = getTile(tempColor);
				if(tile.tag == "Wall") {
					collisionGrid[w,h] = 2;
					winCount--;
					//Update color of object
					//float colorLvl = Random.Range(0.6f, 0.8f);
					//Color temp = new Color(colorLvl, colorLvl, colorLvl);
					//tile.GetComponent<SpriteRenderer>().color = temp;
				}
				if(tile.tag == "BlankTile") {
					collisionGrid[w,h] = 0;
					//Update color of object
					//float colorLvl = Random.Range(0.75f, 1f);
					//Color temp = new Color(colorLvl, colorLvl, colorLvl);
					//tile.GetComponent<SpriteRenderer>().color = temp;

					//Load and set the overlay sprite
					GameObject overlay = Util_Unity.getResource("BGTileOverlay");
					setObjAsTile(overlay, w, h, 0.001f);
				}

				//set the tile object to correct position
				setObjAsTile(tile, w, h, 0);
			}
		}

		//Instantiate analog stick
		GameObject analogStick = (GameObject)Instantiate(Resources.Load("AS_AnalogStickObj"));
		analogStick.GetComponent<AnalogStick>().init(analogWindow.middle(), analogWindow);
		analogStick.transform.localScale = Vector3.one * analogWindow.getWidth() * 0.4f;

		//Remember the location of the 0,0 tile
		originTileLoc = gameWindow.getOrigin() + new Vector3(borderW + curTileSize/2, borderH + curTileSize/2, 0);

		//Instantiate snake
		GameObject snakeObj = (GameObject)Instantiate(Resources.Load("Snake"));
		Snake snake = snakeObj.GetComponent<Snake>();
		int[] bodySegDir = new int[3];
		bodySegDir[0] = 4;
		bodySegDir[1] = 4;
		bodySegDir[2] = 4;
		snake.init(new Vector3(3,0,0), bodySegDir);

		//Instantiate background
		GameObject bg = (GameObject)Instantiate(Resources.Load("Background"));
		Layout.window.stretchSpriteToWindow(bg, 10);

		//Load an apple
		generateApple();
	}



	//
	//Private function that gets a tile based off of a color
	//
	private static GameObject getTile(Color32 color) {
		if(color.r == 0 && color.g == 0 && color.b == 0)
			return (GameObject) Instantiate(Resources.Load("BGTile"));
		if(color.r == 100 && color.g == 100 && color.b == 100)
			return (GameObject) Instantiate(Resources.Load("WallTile"));
		else return null;
	}



	//
	//Private function that sets the desired object to the correct scale and position
	//	of a tile at the specified integer coordinates
	//
	private static void setObjAsTile(GameObject obj, int w, int h, float zOffset) 
	{
		Layout.scaleSprite(obj, new Vector2(curTileSize, curTileSize));
		obj.transform.position = gameWindow.getOrigin() + new Vector3(
			borderW + w * curTileSize + curTileSize/2,
			borderH + h * curTileSize + curTileSize/2, 1 + zOffset);
	}



	//
	//Returns the int ID for the collision type of requested tile
	//
	public static int getCollisionId(int x, int y) {
		return collisionGrid[x,y];
	}



	//
	//Generates an "apple" on an open space
	//
	public static void generateApple(){
		Destroy(apple);
		Vector2 appleLocation = getRandomOpenPosition();
		collisionGrid[(int)appleLocation.x, (int)appleLocation.y] = 3;
		apple = Util.loadResourceGO("apple");
		SnakeUtil.setScaleToTileSize(apple);
		SnakeUtil.setLocation(apple, new Vector3(appleLocation.x, appleLocation.y, 0));
	}



	//
	//Finds a ranodom coordinate pair at which there is currently no collision
	//Preconditions: there must be at least one space on the collisionGrid that is open
	//
	private static Vector2 getRandomOpenPosition(){

		//Loop through all possible cells and check if cell is open
		List<Vector2> coords = new List<Vector2>();
		for(int x = 0; x < collisionGrid.GetLength(0); x++) {
			for(int y = 0; y < collisionGrid.GetLength(1); y++) {

				//If cell is open, store its coordinates in the list
				if(isOpenForApple(x,y))
					coords.Add(new Vector2(x,y));
			}
		}

		//return a random set of coordinates from the list
		return coords[ Random.Range(0, coords.Count - 1) ];
	}



	//
	//Checks if tile is open for the "apple" to be generated here
	//
	private static bool isOpenForApple(int x, int y) {
		return collisionGrid[x,y] == 0;
	}



	//
	//Allows easy access to mark collision ID's
	//
	public static void setCollisionID(int x, int y, int value) {
		collisionGrid[x,y] = value;
	}
}
