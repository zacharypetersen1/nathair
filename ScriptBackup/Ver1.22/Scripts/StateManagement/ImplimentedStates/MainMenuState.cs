﻿using UnityEngine;
using System.Collections;

public class MainMenuState : State {



	Button startButton;			//Will launch the gamestate
	Button levelsButton;		//Will launch world select screen
	


	//
	// Initializes main menu
	//
	public override void enter() {
		GameObject startButtonGO = (GameObject) Instantiate(Resources.Load("StartButton"));
		startButtonGO.transform.position = new Vector3(Layout.unitWidth/2, Layout.unitHeight/2);
		startButton = startButtonGO.GetComponent<Button>();
		GameObject levelsButtonGO = (GameObject) Instantiate(Resources.Load("LevelsButton"));
		levelsButtonGO.transform.position = new Vector3(Layout.unitWidth/2, Layout.unitHeight/4);
		levelsButton = levelsButtonGO.GetComponent<Button>();
	}



	//
	//Restores the main menu
	//
	public override void restore() {
		GameObject startButtonGO = (GameObject) Instantiate(Resources.Load("StartButton"));
		startButtonGO.transform.position = new Vector3(Layout.unitWidth/2, Layout.unitHeight/2);
		startButton = startButtonGO.GetComponent<Button>();
		GameObject levelsButtonGO = (GameObject) Instantiate(Resources.Load("LevelsButton"));
		levelsButtonGO.transform.position = new Vector3(Layout.unitWidth/2, Layout.unitHeight/4);
		levelsButton = levelsButtonGO.GetComponent<Button>();
	}

	

	//
	//Updates Main Menu
	//
	public override void manualUpdate() {
		startButton.manualUpdate();
		levelsButton.manualUpdate();
	}



	//
	//Hides the main menu
	//
	public override void hide() {
		Destroy(startButton.gameObject);
		Destroy(levelsButton.gameObject);
	}



	//
	//Removes Main Menu
	//
	public override void leave() {
		Destroy(startButton.gameObject);
		Destroy(levelsButton.gameObject);
	}
}
