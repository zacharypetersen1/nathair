﻿using UnityEngine;
using System.Collections;

public class LevelSelectState : State {



	Button[] buttons = new Button[13];			//Holds all the level select buttons



	//
	// Initializes main menu
	//
	public override void enter() {

		float[] layoutInfo = Layout.findOptimalMenuLayout(3, 4, 0.2f, 0.2f);

		int counter = 0;
		for(int h = 2; h >= 0; h--) {
			for(int w = 0; w < 4; w++) {
				counter++;
				GameObject levelButton = (GameObject) Instantiate(Resources.Load("LevelSelectButton"));
				levelButton.transform.position = new Vector3(layoutInfo[0] + layoutInfo[4] * w, layoutInfo[1] + layoutInfo[5] * h, 0);
				levelButton.transform.localScale = new Vector3(layoutInfo[2] / 32, layoutInfo[3] / 16, 1);
				buttons[counter] = levelButton.GetComponent<Button>();
				buttons[counter].idle = LevelSpriteStore.getUnselectedLvlSprite(1, counter);
				buttons[counter].GetComponent<SpriteRenderer>().sprite = buttons[counter].idle;
				buttons[counter].cursorDown = LevelSpriteStore.getSelectedLvlSprite(counter);
				buttons[counter].GetComponent<StartLevelEvent>().setLevel(1, counter);
			}
		}
	}



	//
	//Restores the main menu
	//
	public override void restore() {

		float[] layoutInfo = Layout.findOptimalMenuLayout(3, 4, 0.2f, 0.2f);
		
		int counter = 0;
		for(int h = 2; h >= 0; h--) {
			for(int w = 0; w < 4; w++) {
				counter++;
				GameObject levelButton = (GameObject) Instantiate(Resources.Load("LevelSelectButton"));
				levelButton.transform.position = new Vector3(layoutInfo[0] + layoutInfo[4] * w, layoutInfo[1] + layoutInfo[5] * h, 0);
				levelButton.transform.localScale = new Vector3(layoutInfo[2] / 32, layoutInfo[3] / 16, 1);
				buttons[counter] = levelButton.GetComponent<Button>();
				buttons[counter].idle = LevelSpriteStore.getUnselectedLvlSprite(1, counter);
				buttons[counter].GetComponent<SpriteRenderer>().sprite = buttons[counter].idle;
				buttons[counter].cursorDown = LevelSpriteStore.getSelectedLvlSprite(counter);
			}
		}

	}

	

	//
	//Updates Main Menu
	//
	public override void manualUpdate() {
		for(int i = 1; i <= 12; i++) {
			buttons[i].manualUpdate();
		}
	}



	//
	//Hides the main menu
	//
	public override void hide() {
		for(int i = 1; i <= 12; i++) {
			Destroy(buttons[i].gameObject);
		} 
	}



	//
	//Removes Main Menu
	//
	public override void leave() {
		for(int i = 1; i <= 12; i++) {
			Destroy(buttons[i].gameObject);
		} 
	}

}
