﻿//
//Stack State objects implimented with a doubly linked list
//
public class StateStack{



	private Node last = null;	//end of the stack



	//
	//Node for the linked list
	//
	private class Node{

		public Node next;
		public Node prev;
		public State val;

		public Node(State newState) {
			val = newState;	
		}
	}



	//
	//Checks if the stack is empty
	//
	public bool empty() {
		return last == null;
	}



	//
	//Adds newState onto the back of the stack
	//
	public void push(State newState) {
		Node newNode = new Node(newState);
		if(!this.empty())
			last.next = newNode;
		newNode.prev = last;
		last = newNode;
	}



	//
	//Removes the state on top of the stack
	//
	public void pop() {
		if(!this.empty()) {
			if(last.prev != null) {
				last.prev.next = null;
			}
			Node temp = last.prev;
			last.prev = null;
			last = temp;
		}
	}



	//
	//Returns the top state of the stack
	//
	public State peek() {
		return last.val;
	}
}
