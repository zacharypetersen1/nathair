﻿using UnityEngine;
using System.Collections;



//
//Base class for all states
//
public abstract class State : MonoBehaviour {



	//
	//Put state initilization code here
	//
	public abstract void enter();



	//
	//Put code for "restoring" state here (meaning that state was hidden and then is now being returned to)
	//
	public abstract void restore();



	//
	//Put update code for state here
	//
	public abstract void manualUpdate();



	//
	//Put code for "hiding" state here (meaning that we're moving to another state but not destroying this one)
	//	-like going from game state to pause menu
	//
	public abstract void hide();

	

	//
	//Put state destruction code here
	//
	public abstract void leave();
}
