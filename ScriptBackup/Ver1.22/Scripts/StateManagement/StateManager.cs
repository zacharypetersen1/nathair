﻿using UnityEngine;
using System.Collections;

public class StateManager : MonoBehaviour {
	


	private static StateStack states;	//stack of all currently opened states (only the top state is active)



	//
	// Main initialization function for the game
	//
	void Start () {
		states = new StateStack();
		addState("MainMenuState");
	}



	//
	// Main update function for game, calls update of the current game state
	//
	void Update () {
		states.peek().manualUpdate();
	}



	//
	//Use this function to add new state to state stack
	//Automatically calls the enter function in the new state
	//
	public static void addState(string type){

		//
		//Create the new state
		//
		GameObject newState = (GameObject)Instantiate(Resources.Load(type));

		//
		//If there is a previous state, make sure to call its 'hide' function
		//
		if(!states.empty())
			states.peek().hide();

		//
		//Push new state onto the stack and call its 'enter' function
		//
		states.push(newState.GetComponent<State>());
		states.peek().enter();
	}
}
