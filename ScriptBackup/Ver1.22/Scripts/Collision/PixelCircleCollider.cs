﻿using UnityEngine;
using System.Collections;

public class PixelCircleCollider : MonoBehaviour {



	float radius;			//Stores the radius (in pixels) of the sprite
	float pixelsPerUnit;	//Stores the pixles per unit of this sprite
	
	
	
	//
	//Initialize by finding the dimensions of the sprite
	//
	void Start() {
		Sprite temp = gameObject.GetComponent<SpriteRenderer>().sprite;
		radius = temp.texture.width / 2f;
		pixelsPerUnit = temp.pixelsPerUnit;
	}
	
	
	
	//
	//Call this method to see if a 2D location is colliding with the Sprite
	//
	//Preconditions:
	//Sprite must be scaled one to one (i.e. x and y scale values are the same)
	//Image in sprite must be circular an roughly take up the whole sprite
	//
	public bool checkPointCollision(Vector2 point) {
		float actualRadius = radius * transform.localScale.y / pixelsPerUnit;
		float distance = Vector2.Distance(point, new Vector2(transform.position.x, transform.position.y));
		return distance <= actualRadius;
	}
}
