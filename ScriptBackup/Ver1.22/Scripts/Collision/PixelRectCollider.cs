﻿using UnityEngine;
using System.Collections;



//
//Attach this to a sprite object for easy collision checking by function call
//Preconditions for Usage:
//	GameObject must also have a SpriteRenderer with valid texture attached
//	Sprite must be centered (i.e. origin is in the center of the sprite)
//	Scale of the sprite cannot be normalized
//	PixelsPerUnit of the sprite must be set to 1
//	Sprite is being scaled on the X/Y plane and is flush with the camera
//
public class PixelRectCollider : MonoBehaviour {



	Vector2 pixelSize;		//Stores the size (in pixels) of the sprite



	//
	//Initialize by finding the dimensions of the sprite
	//
	void Start() {
		Sprite temp = gameObject.GetComponent<SpriteRenderer>().sprite;
		int width = temp.texture.width;
		int height = temp.texture.height;
		pixelSize = new Vector2(width, height);
	}



	//
	//Call this method to see if a 2D location is colliding with the Sprite
	//
	public bool checkPointCollision(Vector2 point) {
		float actualWidth = pixelSize.x * transform.localScale.x;
		float actualHeight = pixelSize.y * transform.localScale.y;
		if(point.x >= transform.position.x - actualWidth/2)
			if(point.x <= transform.position.x + actualWidth/2)
				if(point.y >= transform.position.y - actualHeight/2)
					if(point.y <= transform.position.y + actualHeight/2)
						return true;
		return false;
	}
}
