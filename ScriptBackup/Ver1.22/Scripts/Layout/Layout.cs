﻿using UnityEngine;
using System.Collections;



public class Layout : MonoBehaviour {



	public static float unitHeight;	//Actual height in unity units of the screen
	public static float unitWidth;	//Actual width in unity units of the screen
	public static Window window;	//full window of the screen



	//
	//
	//
	void Awake () {
		//Get the width and height of screen in unity units
		Layout.unitHeight = Camera.main.orthographicSize * 2.0f;
		Layout.unitWidth  = Camera.main.aspect * Layout.unitHeight;
		window = new Window();
		window.setWidth(unitWidth);
		window.setHeight(unitHeight);
		window.setOrigin(Vector3.zero);
	}



	//
	// Use this for initialization
	//
	void Start () {

	}



	//
	//Run this to get the info for setting up a layout of buttons in a set amount of rows and columns
	//	For exampls, this function is used to set up the level select screen
	//	Returns array of information in this order:
	//	0: Origin X of the bottom left button
	//	1: Origin Y of the bottom right button
	//	2: Size for Width of the buttons 
	//	3: Size for Height of the buttons
	//	4: Amount of offset X from one button to the other
	//	5: Amount of offset Y from one button to the other
	//
	public static float[] findOptimalMenuLayout(int numRows, int numColumns,
	                                            float percentWhitespace,
	                                            float percentMargins) {

		//Set up array
		float[] temp = new float[6];

		//Calculate optimal margin sizes for the width and height
		float marginSizeW = Layout.unitWidth * percentMargins / 2;
		float marginSizeH = Layout.unitHeight * percentMargins / 2;

		//Calculate optimal button sizes for width and height
		float buttonWidth = Layout.unitWidth * (1 - percentMargins - percentWhitespace) / numColumns;
		float buttonHeight = Layout.unitHeight * (1 - percentMargins - percentWhitespace) / numRows;

		//Check to which dimension (height or width) is the limiting factor
		//Recalculate button and margin sizes accordingly
		float buttonMargin = 0;
		if(buttonWidth > buttonHeight*2) {
			buttonWidth = buttonHeight*2;
			buttonMargin = ((Layout.unitHeight - (2 * marginSizeH)) - (buttonHeight * numRows)) / (numRows + 1);
			marginSizeW = (Layout.unitWidth - (buttonWidth * numColumns + buttonMargin * (numColumns + 1)) ) / 2;
		}
		else if(buttonWidth/2 < buttonHeight) {
			buttonHeight = buttonWidth/2;
			buttonMargin = ((Layout.unitWidth - (2 * marginSizeW)) - (buttonWidth * numColumns)) / (numColumns + 1);
			marginSizeH = (Layout.unitHeight - (buttonHeight * numRows + buttonMargin * (numRows + 1)) ) / 2;
		}

		//Calculate origin location for the (0,0) button
		float originX = marginSizeW + buttonMargin + buttonWidth / 2;
		float originY = marginSizeH + buttonMargin + buttonHeight / 2;

		//Calculate the ammount of offset that will exist between buttons
		float buttonOffsetX = buttonWidth + buttonMargin;
		float buttonOffsetY = buttonHeight + buttonMargin;

		//Pack info into array for returning
		temp[0] = originX;
		temp[1] = originY;
		temp[2] = buttonWidth;
		temp[3] = buttonHeight;
		temp[4] = buttonOffsetX;
		temp[5] = buttonOffsetY;
		return temp;
	}



	//
	//Scales the sprite to desired scale regardless of pixels per unit settings
	//
	public static void scaleSprite(GameObject obj, Vector2 scale) {
		Sprite sprite = obj.GetComponent<SpriteRenderer>().sprite;
		float pixPerUnit = sprite.pixelsPerUnit;
		obj.transform.localScale = new Vector3(scale.x * pixPerUnit / (float) sprite.texture.width,
		                                       scale.y * pixPerUnit / (float) sprite.texture.height, 1);
	}
}
