using UnityEngine;
using System.Collections;



//
//Stores a set of values and provides a set of utility functions that make
//	it simple to define a two dimensional window
//
public class Window
{
	


	private float unitWidth;	//width in units of the window
	private float unitHeight;	//height in units of the window
	private Vector3 origin;		//bottom left origin of the window



	//
	//Manipulator functions
	//
	public void setWidth(float width) {
		unitWidth = width;
	}

	public void setHeight(float height) {
		unitHeight = height;
	}

	public void setOrigin(Vector3 setToThis) {
		origin = setToThis;
	}



	//
	//Accessor functions
	//
	public float getWidth() {
		return unitWidth;
	}

	public float getHeight() {
		return unitHeight;
	}

	public Vector3 getOrigin() {
		return origin;
	}

	//Returns the maximum point rather than the minimum point of the window
	public Vector3 getOriginInverse() {
		return new Vector3(origin.x + unitWidth, origin.y + unitHeight, origin.z);
	}


	//
	//Returns middle location of the window
	//
	public Vector3 middle() {
		return origin + new Vector3(unitWidth/2f, unitHeight/2f, 0);
	}



	//
	//Scales and places the object to fit over the window
	//	Preconditions: object must be a sprite
	//
	public void stretchSpriteToWindow(GameObject thisObject, float offsetZ) {
		thisObject.transform.position = middle() + new Vector3(0,0,offsetZ);
		Sprite sprite = thisObject.GetComponent<SpriteRenderer>().sprite;
		float scaleX = unitWidth / (float)sprite.texture.width * sprite.pixelsPerUnit;
		float scaleY = unitHeight / (float)sprite.texture.height * sprite.pixelsPerUnit;
		thisObject.transform.localScale = new Vector3(scaleX, scaleY, 1);
	}



	//
	//Sets a pattern to fit over the window exactly
	//	Preconditions: object must be a sprite
	//
	public void stretchPatternToWindow(string fillObject, string edgeObject, string cornerObject,
	                                   int numPatternsW, int numPatternsH, float offsetZ) {

		if(numPatternsW <= 0 && numPatternsH <= 0) {
			throw new UnityException("Either pattern number width or pattern number height must be greater than 0 to call stretchPatternToWindow");
		}

		int numW, numH;
		float sizeW, sizeH;

		//Find the correct size of pattern tiles
		if(numPatternsH <= 0) {
			numW = numPatternsW;
			sizeW = unitWidth / numW;
			numH = (int)Mathf.Round(unitHeight/sizeW);
			sizeH = unitHeight / numH;
		}
		else if(numPatternsW <= 0) {
			numH = numPatternsH;
			sizeH = unitHeight / numH;
			numW = (int)Mathf.Round(unitWidth/sizeH);
			sizeW = unitWidth / numW;
		}
		else{
			numW = numPatternsW;
			sizeW = unitWidth / numW;
			numH = numPatternsH;
			sizeH = unitHeight / numH;
		}

		//Instantiate the pattern and set to correct locations
		for(int w = 0; w < numW; w++) {
			for(int h = 0; h < numH; h++) {

				//Check for corner Cases
				if((w == 0 || w == numW-1) && (h == 0 || h == numH-1)){
					GameObject temp = Util.loadResourceGO(cornerObject);
					Layout.scaleSprite(temp, new Vector2(sizeW, sizeH));
					temp.transform.position = new Vector3(w * sizeW + sizeW/2, h * sizeH + sizeH / 2, offsetZ);
					if(w == 0){
						if(h == 0)
							temp.transform.Rotate(Vector3.forward * 90);
					}
					else
					{
						if(h == 0)
							temp.transform.Rotate(Vector3.forward * 180);
						else
							temp.transform.Rotate(Vector3.forward * 270);
					}
				}

				//Check for 'edge' (i.e. edge of the pattern) cases
				else if(w == 0 || w == numW-1 || h == 0 || h == numH-1) {
					GameObject temp = Util.loadResourceGO(edgeObject);
					Layout.scaleSprite(temp, new Vector2(sizeW, sizeH));
					temp.transform.position = new Vector3(w * sizeW + sizeW/2, h * sizeH + sizeH / 2, offsetZ);
					if(w == 0)
						temp.transform.Rotate(Vector3.forward * 90);
					else if(h == 0)
						temp.transform.Rotate(Vector3.forward * 180);
					else if(w == numW-1)
						temp.transform.Rotate(Vector3.forward * 270);
					else{
						//No rotation
					}
				}

				//Handle the 'fill' case
				else {
					GameObject temp = Util.loadResourceGO(fillObject);
					Layout.scaleSprite(temp, new Vector2(sizeW, sizeH));
					temp.transform.position = new Vector3(w * sizeW + sizeW/2, h * sizeH + sizeH / 2, offsetZ);
				}
			}
		}
	}



	//
	//Checks if the given point is within the bounds of the window (2D only on the X/Y plane)
	//
	public bool checkCollision(Vector3 point) {
		return ((point.x >= origin.x) && (point.x <= getOriginInverse().x)
		        && (point.y >= origin.y) && (point.y <= getOriginInverse().y));
	}
}

