using UnityEngine;
using System.Collections;

public class Util_Unity
{
	//Simplifies the process of copying a resource object
	public static GameObject getResource(string path) {
		return (GameObject) MonoBehaviour.Instantiate(
			Resources.Load(path));
	}
}

