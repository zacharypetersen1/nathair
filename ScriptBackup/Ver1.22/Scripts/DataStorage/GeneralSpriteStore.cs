﻿using UnityEngine;
using System.Collections;

public class GeneralSpriteStore : MonoBehaviour {


	
	public Sprite[] snakeStrait = new Sprite[24];		//stores the sprites for snake head that follow face
	public Sprite[] snakeStraitBack = new Sprite[24];	//stores the sprites for snake head that follow face
	public Sprite[] snakeClockWise = new Sprite[24];
	public Sprite[] snakeClockWiseBack = new Sprite[24];
	public Sprite[] snakeCounterClockWise = new Sprite[24];
	public Sprite[] snakeCounterClockWiseBack = new Sprite[24];
	public Sprite SNAKE_HEAD;							//Stores face sprite of snake
	public Sprite SNAKE_STRAIT;
	public Sprite SNAKE_CLOCKWISE;
	public Sprite SNAKE_COUNTERCLOCKWISE;

	public static GeneralSpriteStore store;



	//
	//Destroy self if instance already exists
	//
	void Awake () {
		if(store != null)
			Destroy(gameObject);

		store = this;
	}



	//
	//Returns sprite for snake strait at specified percent of the interval
	//
	public Sprite getSnakeStraitSprite(float percent) {
		percent *= snakeStrait.GetLength(0);
		return snakeStrait[(int) percent];
	}

	//
	//Returns sprite for snake strait back at specified percent of the interval
	//
	public Sprite getSnakeStraitSpriteB(float percent) {
		percent *= snakeStraitBack.GetLength(0);
		return snakeStraitBack[(int) percent];
	}
	
	//
	//Returns sprite for snake clockwise segment at specified percent of the interval
	//
	public Sprite getSnakeClockwiseSprite(float percent) {
		percent *= snakeClockWise.GetLength(0);
		return snakeClockWise[(int) percent];
	}

	//
	//Returns sprite for snake clockwise back segment at specified percent of the interval
	//
	public Sprite getSnakeClockwiseSpriteB(float percent) {
		percent *= snakeClockWiseBack.GetLength(0);
		return snakeClockWiseBack[(int) percent];
	}

	//
	//Returns sprite for snake counterclockwise at specified percent of the interval
	//
	public Sprite getSnakeCounterClockwiseSprite(float percent) {
		percent *= snakeCounterClockWise.GetLength(0);
		return snakeCounterClockWise[(int) percent];
	}

	//
	//Returns sprite for snake counterclockwise at specified percent of the interval
	//
	public Sprite getSnakeCounterClockwiseSpriteB(float percent) {
		percent *= snakeCounterClockWiseBack.GetLength(0);
		return snakeCounterClockWiseBack[(int) percent];
	}
}
