using UnityEngine;
using System.Collections;


//
//Singleton class for storing and getting level select sprites
//
//Execution Time: Before Default Time (first)
//
public class LevelSpriteStore : MonoBehaviour
{
	public Sprite[] world1 = new Sprite[13];
	public Sprite[] world2 = new Sprite[13];
	public Sprite[] selected = new Sprite[13];

	public static LevelSpriteStore store;



	//
	//Destroy self if instance already exists
	//
	void Awake() {
		if(store != null)
			Destroy(gameObject);
		else
			store = this;
	}



	//
	//Returns unselected level sprite for specified level
	//
	public static Sprite getUnselectedLvlSprite(int world, int level) {

		switch(world) {
		case 1: return store.world1[level];
		case 2: return store.world2[level];
		}

		return null;
	}



	//
	//Returns selected level sprite for specified level
	//
	public static Sprite getSelectedLvlSprite(int level) {
		return store.selected[level];
	}
}

