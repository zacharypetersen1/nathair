using UnityEngine;
using System.Collections;



//
//Creates a Button that can cause events to happen when pressed and realeased
//Preconditions:
//	Must also have a rectCollider component attached (meaning that rectCollider preconditions must be met)
//	Must provide an idle and cursorDown sprite
//	Must have a component that extends the EventFunction class (this script will be triggered by the button)
//
public class Button : MonoBehaviour {



	public Sprite idle;				//Sprite for idle state
	public Sprite cursorDown;		//Sprite for active state (pressed but not realeased)
	bool isActive = false;			//Flag for active state
	SpriteRenderer spriteRenderer;	//Easy access for switching sprites
	PixelRectCollider rectCollider;	//Easy access for collision checking function calls



	//
	// Use this for initialization
	//
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		rectCollider = gameObject.GetComponent<PixelRectCollider>();
	}



	// Update is called once per frame
	public void manualUpdate () {

		//
		//If currently active, check if cursor left button area
		//
		if(isActive) {
			if(!rectCollider.checkPointCollision(Mouse.worldMouse2D)) {
				spriteRenderer.sprite = idle;
				isActive = false;
			}
		}

		//
		//If mouse down, check if within button area- if so set to active
		//
		if(Input.GetMouseButtonDown(0)) {
			if(rectCollider.checkPointCollision(Mouse.worldMouse2D)) {
				spriteRenderer.sprite = cursorDown;
				isActive = true;
				gameObject.GetComponent<ButtonEvent>().triggerDown();
			}
		}

		//
		//If mouse up, check if currently active- if so trigger event
		//
		if(Input.GetMouseButtonUp(0)) {
			if(isActive) {
				gameObject.GetComponent<ButtonEvent>().triggerUp();
			}
			spriteRenderer.sprite = idle;
			isActive = false;
		}
	}
}
