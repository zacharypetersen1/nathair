﻿using UnityEngine;
using System.Collections;

public abstract class ButtonEvent : MonoBehaviour {
	


	//
	// This method is invoked when button is first pressed down
	//
	public virtual void triggerDown(){

	}



	//
	// This method is invoked when button is first released
	//
	public virtual void triggerUp(){

	}
}
