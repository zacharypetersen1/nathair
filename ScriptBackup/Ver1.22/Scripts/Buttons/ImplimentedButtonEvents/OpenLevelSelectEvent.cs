using UnityEngine;
using System.Collections;

public class OpenLevelSelectEvent: ButtonEvent {



	public int worldNumber = 1;		//used to determine which level this event will load



	//
	//Time to start the game- add game state to the state stack
	//
	public override void triggerUp(){
		StateManager.addState("LevelSelectState");
	}



	//
	//Use this to set world and level
	//
	public void setWorld(int world){
		worldNumber = world;
	}

}
