using UnityEngine;
using System.Collections;

public class StartLevelEvent : ButtonEvent {



	public int worldNumber = 1;		//used to determine which level this event will load
	public int levelNumber = 3;



	//
	//Time to start the game- add game state to the state stack
	//
	public override void triggerUp(){
		StateManager.addState("GameState");
		Levels.loadLevel(worldNumber, levelNumber);
	}



	//
	//Use this to set world and level
	//
	public void setLevel(int world, int level){
		worldNumber = world;
		levelNumber = level;
	}

}
