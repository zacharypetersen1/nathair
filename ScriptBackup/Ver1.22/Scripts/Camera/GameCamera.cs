﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {



	public static GameObject gameCameraObject;



	//
	//Awake
	//
	void Awake () {
		gameCameraObject = gameObject;
	}



	//
	// Use this for initialization
	//
	void Start () {
		//Move camera to the correct position so that origin is in bottom left of screen
		transform.position = new Vector3(Layout.unitWidth/2, Layout.unitHeight/2, -1);
	}



	//
	// Update is called once per frame
	//
	void Update () {
	
	}
}
