using UnityEngine;
using System.Collections;


public class Snake : MonoBehaviour {


	
	public Vector3 headLocation;	//the 2-coordinate location refering to what tile the snake is on
	public float maxTileTime = 1;		//the amount of time that is spent moving from one end of a tile to the other
	private GameObject frontAnimation;	//GameObject used to animated the front of the snakeQueue
	private Vector2 frontAnimationInfo;	//Stores information used to determine orientation of front animation
	private float curTileTime;			//the amount of time that has passed since beginning to move across the current tile
	private float offsetScalar;			//The amount of offset from the center of the tile (calculated from the intervalTi
	private int pastDirection = 0;		//The direction taken to get to this tile from the last tile
	private int nextDirection = 0;		//The direction that will be taken to procede to the next stuff
	private float dirRestriction = 0.8f;//A value normalized between 0-1 that represents the time window on this tile during which direction can be changed
	private int carryOverDirection = 0;	//Carrys over direction value from the past tile if direction was changed outside of "dirRestriction time window"
	private bool isCarryOver = false;	//Records if there is a carry over
	public SnakeCircleQueue snakeQueue;
	private bool wasHalfStep = false;	//Records if reached and triggered halfStep
	private int addSegmentCount = 0;	//Records how many segments need to be added from eating apple



	// Use this for initialization
	void Start () {

	}



	//
	// Update is called once per frame
	//
	void Update () {
		if(Input.GetKey(KeyCode.R))
			move (-Time.deltaTime);
		else
			move (Time.deltaTime);
	}//End of Update



	//
	//Initializes snake
	//Pass int the location of the head, then a array of 'direction' ints (0,2,4,or 6)
	//	indicating which direction each subsequent body segment prodrudes from the snake
	//
	public void init(Vector3 setHeadLocation, int[] bodySegDirections) {

		//Scale the head object (which this script is attached to)
		SnakeUtil.setScaleToTileSize(gameObject);

		//Move the head to the correct locaiton
		headLocation = setHeadLocation;
		SnakeUtil.setLocation(gameObject, headLocation);

		//Rotate the head in the correct direction
		SnakeUtil.setRotation(gameObject, SnakeUtil.invertDirection(bodySegDirections[0]));

		//Mark this position's collision
		Levels.setCollisionID((int)headLocation.x, (int)headLocation.y, 1);

		//Initialize the snakeQueue
		snakeQueue = new SnakeCircleQueue();

		//Initialize the frontAnimation object
		frontAnimation = SnakeUtil.newBodySegment();

		//create a temporary locational value used to initialize the body segments, begin at the head's location
		Vector3 tempLocation = headLocation;

		//Loop to initialize body segments
		int i;
		for(i = 0; i <= bodySegDirections.Length-1; i++) {

			//Update the temp locational variable in the direction of the next body segment
			tempLocation = SnakeUtil.getLocFromDir(tempLocation, bodySegDirections[i]);

			//Create a gameObject for the body segment and set its location and rotation
			GameObject tempObj = SnakeUtil.newBodySegment();
			SnakeUtil.setLocation(tempObj, tempLocation);
			SnakeUtil.setRotation(tempObj, SnakeUtil.invertDirection(bodySegDirections[i]));

			//Mark collision
			Levels.setCollisionID((int)tempLocation.x, (int)tempLocation.y, 1);

			//Add a new node storing the new object in the snakeQueue
			snakeQueue.addSegmentLast(tempObj, SnakeUtil.invertDirection(bodySegDirections[i]) );
		}

		//Set the last pastDirection of snakeCirclQueue
		snakeQueue.setLastSegPrev(/*SnakeUtil.invertDirection( snakeQueue.last ().dirToNext() )*/4);

		//Orient and set sprites for all snake body segments
		snakeQueue.orientAndSetSpriteAll();

		//Move the animation to the correct location
		SnakeUtil.setLocation(frontAnimation, SnakeUtil.getCoords(snakeQueue.first().obj()) );
		SnakeUtil.setRotation(frontAnimation, nextDirection);
		frontAnimationInfo.x = nextDirection;
		frontAnimationInfo.y = SnakeUtil.invertDirection(nextDirection);
	}


	
	//Checks if it is possible to change direction in current movement cycle or if must wait for the next movement cycle
	private bool canChangeDir(){
		return curTileTime/maxTileTime < dirRestriction;
	}



	//
	//Executes the snake movement
	//
	private void move(float deltaTime) {

		//Determine if time is moving forward
		bool timeIsForward = deltaTime > 0;

		//Run this block if time is going forward
		if(timeIsForward){
			//check if an arrow was pressed since last frame, if so update accordingly
			int arrowDownDirection = AnalogStick.analogStick.whichArrowDown();
			if(arrowDownDirection != -1) {
				if(!checkCollision(arrowDownDirection)) {
					if(canChangeDir()) {
						nextDirection = arrowDownDirection;
						if(wasHalfStep){
							frontAnimationInfo.x = nextDirection;
							SnakeUtil.orientSegment(frontAnimation, (int)frontAnimationInfo.x, (int)frontAnimationInfo.y);
						}
					}
					else {
						carryOverDirection = arrowDownDirection;

					}
					SnakeUtil.setRotation(gameObject, arrowDownDirection);
				}
			}
		}

		//Record time change
		curTileTime += deltaTime;

		//Determine if currently in first half of movement cycle or second half
		bool firstHalf = curTileTime < maxTileTime/2;
		
		//Run collision if in second half of movement cycle
		if(!firstHalf) {
			//check if the square that snake is about to move to triggers a collision
			if(checkCollision(nextDirection)) {
				curTileTime = maxTileTime/2;
				transform.position = Levels.originTileLoc + (headLocation * Levels.curTileSize);
				return;
			}
		}

		//Check if completed half of current movement cycle
		if(!wasHalfStep) {
			if(curTileTime >= maxTileTime/2) {
				//Call the halfStepForward function
				halfStepForward();

				//Record that helfStepForward was called
				wasHalfStep = true;
			}
		}

		//Check if completed current movement cycle
		if(curTileTime >= maxTileTime) {

			//If so, call endStep
			endStepForward();

			//Incriment intervalCurrent down
			curTileTime -= maxTileTime;

			//Since entering new movement cycle, record that there has been no halfStep
			wasHalfStep = false;

			//Reset Snake to first-half
			firstHalf = true;
		}

		float animTime = curTileTime/maxTileTime;
		float animTimeHalfOffset = curTileTime/maxTileTime -0.5f;
		if(animTimeHalfOffset < 0)
			animTimeHalfOffset += 1;

		//Update sprite of segment directly behind the head
		SnakeUtil.setSegmentSpriteAnimated(frontAnimation, (int)frontAnimationInfo.x, 
		                                   (int)frontAnimationInfo.y, animTimeHalfOffset);

		//Update sprite of tail (DO NOT UPDATE IF CURRENTLY IN PROCESS OF ADDING SEGMENT)
		if(addSegmentCount == 0)
			SnakeUtil.setSegmentSpriteAnimatedB(snakeQueue.last(), animTime);

		//
		//Now carry out the correct offset (will differ depending if in firstHalf or secondHalf
		//
		SnakeUtil.setLocation(gameObject, headLocation);
		if(firstHalf) {
			offsetScalar = 1 - ( (curTileTime) / (maxTileTime/2) );	//normalize current offset
			switch(pastDirection){
			case 0: transform.Translate(Vector3.left * offsetScalar * Levels.curTileSize/2, Space.World); break;
			case 2: transform.Translate(Vector3.up * offsetScalar * Levels.curTileSize/2, Space.World); break;
			case 4: transform.Translate(Vector3.right * offsetScalar * Levels.curTileSize/2, Space.World); break; 
			case 6: transform.Translate(Vector3.down * offsetScalar * Levels.curTileSize/2, Space.World); break;
			}
		}
		else {
			offsetScalar = (curTileTime-maxTileTime/2) / (maxTileTime/2);
			switch(nextDirection){
			case 0: transform.Translate(Vector3.right * offsetScalar * Levels.curTileSize/2, Space.World); break;
			case 2: transform.Translate(Vector3.down * offsetScalar * Levels.curTileSize/2, Space.World); break;
			case 4: transform.Translate(Vector3.left * offsetScalar * Levels.curTileSize/2, Space.World); break;
			case 6: transform.Translate(Vector3.up * offsetScalar * Levels.curTileSize/2, Space.World); break;
			}
		}
	}//End of move function



	//
	//Executes one time halfway through the step of snake movement when time is flowing forward
	//
	private void halfStepForward() {

		//Move the animation to the location of the snake's head
		SnakeUtil.setLocation(frontAnimation, headLocation);

		//Set initial rotation of the animation GameObject
		SnakeUtil.setRotation(frontAnimation, nextDirection);

		//Show first body segment sprite since animation has moved and no longer obstructs first body segment
		SpriteUtil.setSpriteVisibility(snakeQueue.first().obj(), true);

		//Find direction to the last body part
		frontAnimationInfo.y = SnakeUtil.getDirection(SnakeUtil.getCoords(frontAnimation),
		                                              SnakeUtil.getCoords(snakeQueue.first().obj()) );
		frontAnimationInfo.x = nextDirection;
		SnakeUtil.orientSegment(frontAnimation, (int)frontAnimationInfo.x, (int)frontAnimationInfo.y);

	}



	//
	//Executes and the end of each step of the snake movement cycle
	//(i.e. executes at the end of moving one square)
	//
	private void endStepForward() {

		//Check if need to add a new segment thanks to addSegmentCount
		if(addSegmentCount != 0){
			//do add segment here
			addSegmentCount --;
			snakeQueue.cycleForward(true);
		}
		else{

		//remove collision at old location of tail
		Vector3 tailLocation = SnakeUtil.getCoords(snakeQueue.last().obj());
			Levels.setCollisionID(Mathf.RoundToInt(tailLocation.x), Mathf.RoundToInt(tailLocation.y), 0);
		
		//Update body segment circle queue
		snakeQueue.cycleForward(false);

		}

		//Move new 'first' of circle queue onto location of head before updating head
		SnakeUtil.setLocation(snakeQueue.first().obj(), headLocation);
		
		//Update the the directional information inside 'first' node
		snakeQueue.first().setDirToNext(nextDirection);
		
		//Update orientation of segment by calling orientSegment
		SnakeUtil.orientSegment(snakeQueue.first());

		//Finalize sprite of first body segment
		SnakeUtil.setSegmentSprite(snakeQueue.first());

		//Hide the first Sprite since it obstructs the animation at this stage
		SpriteUtil.setSpriteVisibility(snakeQueue.first().obj(), false);

		//Update Location Of head and set collision at new location of head
		headLocation = SnakeUtil.getLocFromDir(headLocation, nextDirection);
		SnakeUtil.setLocation(gameObject, headLocation);

		//See if apple was eaten
		if(Levels.getCollisionId((int)headLocation.x, (int)headLocation.y) == 3){
			Levels.generateApple();
			addSegmentCount +=3;
		}

		//Update collision grid for the snake's head location
		Levels.setCollisionID((int)headLocation.x, (int)headLocation.y, 1);
		
		//Update pastDirection
		pastDirection = nextDirection;
		
		//Set next dir to carry over dir in case there was a carry over
		if(isCarryOver) {
			nextDirection = carryOverDirection;
			isCarryOver = false;
		}
		
		//Check if any arrows were held down and update nextDirection to match them
		int whichDirPressed = AnalogStick.analogStick.whichArrowPressed();
		if(whichDirPressed != -1) {
			if(!checkCollision(whichDirPressed)){
				nextDirection = whichDirPressed;
				SnakeUtil.setRotation(gameObject, whichDirPressed);
			}
		}

	}//End of endStep function
	
	
	
	//
	//Abstracts collision check command to make it simpler to call in this file
	//
	private bool checkCollision(int dir) {
		return SnakeUtil.checkCollision(headLocation, dir);
	}



	//
	//Checks if a button is being held and updates nextdirection to match that button
	//
	private bool checkPress(){
		if(AnalogStick.analogStick.arrowPressed(0)) {
			if(!checkCollision(0)) {
				nextDirection = 0;
				carryOverDirection = 0;
				SnakeUtil.setRotation(gameObject, 0);
				return true;
			}
		}
		if(AnalogStick.analogStick.arrowPressed(2)) {
			if(!checkCollision(2)) {
				nextDirection = 2;
				carryOverDirection = 2;
				SnakeUtil.setRotation(gameObject, 2);
				return true;
			}
		}
		if(AnalogStick.analogStick.arrowPressed(4)) {
			if(!checkCollision(4)) {
				nextDirection = 4;
				carryOverDirection = 4;
				SnakeUtil.setRotation(gameObject, 4);
				return true;
			}
		}
		if(AnalogStick.analogStick.arrowPressed(6)) {
			if(!checkCollision(6)) {
				nextDirection = 6;
				carryOverDirection = 6;
				SnakeUtil.setRotation(gameObject, 6);
				return true;
			}
		}
		return false;
	}//End of checkPress function
}//End of class
