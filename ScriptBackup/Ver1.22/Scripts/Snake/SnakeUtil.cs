using UnityEngine;
using System.Collections;

public class SnakeUtil : MonoBehaviour
{



	//
	//Returns a scaled gameObject with the correct setup to be a body segment object
	//
	public static GameObject newBodySegment() {
		//Create a gameObject for the body segment and set its location and scale
		GameObject tempObj = (GameObject) Instantiate(Resources.Load("SnakeBodySegment"));
		SnakeUtil.setScaleToTileSize(tempObj);
		return tempObj;
	}



	public static void matchBodySegment(GameObject matchThis, GameObject toThis) {
		matchThis.transform.position = toThis.transform.position;
		matchThis.transform.rotation = toThis.transform.rotation;
	}
	
	
	
	//
	//Rotates object to match desired direction (assumes all sprites drawn with forward facing up)
	//
	public static void setRotation(GameObject obj, int dir) {
		obj.transform.Rotate(-obj.transform.rotation.eulerAngles);
		switch(dir){
		case 0: obj.transform.Rotate(0,0,270); break;
		case 2:	obj.transform.Rotate(0,0,180); break;
		case 4:	obj.transform.Rotate(0,0,90);  break;
		//if direction is 6, leave rotation at 0
		}
	}



	//
	//Returns of the coordinates of the tile that is ofset by one in the given direction from the given starting point
	//
	public static Vector3 getLocFromDir(Vector3 currentLoc, int dir){

		//
		//incriment correct location by one to get find the desired tile location
		//
		switch(dir){
		case(0):currentLoc.x++; break;
		case(2):currentLoc.y--; break;
		case(4):currentLoc.x--; break;
		case(6):currentLoc.y++; break;
		}

		return currentLoc;
	}



	//
	//Checks if there will be collision in specified direction
	//
	public static bool checkCollision(Vector2 currentLoc, int dir) {
		Vector3 newLoc = SnakeUtil.getLocFromDir(currentLoc, dir);
		return checkOutOfLevel(currentLoc, dir)
			|| checkCollisionAtLoc((int)newLoc.x, (int)newLoc.y);
	}



	//
	//Checks if there will be a collision is specified location
	//
	public static bool checkCollisionAtLoc(int x, int y) {
		int value = Levels.getCollisionId(x, y);
		switch(value){
		case(0):
		case(3):
			return false;
		default:
			return true;
		}
	}



	//
	//checks if moving in the specified direction will move out of frame
	//
	private static bool checkOutOfLevel(Vector2 currentLoc, int dir) {
		Vector3 newLoc = SnakeUtil.getLocFromDir(currentLoc, dir);
		return(newLoc.x < 0 || newLoc.y < 0
		       || newLoc.x > Levels.tileCountW - 1
		       || newLoc.y > Levels.tileCountH - 1
		       );
	}



	//
	//Inverts the int representation of direction, for example, 4 becomes 0
	//
	public static int invertDirection(int invertThis) {
		invertThis += 4;
		if(invertThis >= 8)
			invertThis -= 8;
		return invertThis;
	}



	//
	//Determines the direction nesissary to move from the starting location to the new location
	//
	public static int getDirection(Vector3 from, Vector3 to){
		//Determine if this is horizontal or vertical movement
		if(from.y == to.y){
			//Determine if the horizontal movement is in the positive or negative direction
			if(from.x < to.x)
				return 0;
			else
				return 4;
		}
		else {
			//Determine if the vertical movement is in the positive or negative direction
			if(from.y > to.y)
				return 2;
			else
				return 6;
		}
	}


	//
	//Sets location of a snake body segment in actual unity units using a two corrdinate location
	//
	public static void setLocation(GameObject segment, Vector3 location) {
		segment.transform.position = Levels.originTileLoc + (location * Levels.curTileSize);
	}



	//
	//Converts the actual unity units location of segment into coordinates stored in a Vector3
	//
	public static Vector3 getCoords(GameObject segment){
		return (segment.transform.position - Levels.originTileLoc) / Levels.curTileSize;
	}



	//
	//Scales body segments to correct size
	//
	public static void setScaleToTileSize(GameObject segment) {
		Layout.scaleSprite(segment, new Vector2(Levels.curTileSize, Levels.curTileSize));
	}



	//
	//updates the sprite of body segment to match the directions passed as parameters
	//
	public static void orientSegment(SnakeCircleQueue.BodySegment segment) {

		//if lastdir - nextdir == 4, this must be a strait body segment
		if(isStrait(segment)) {
			//Orient object as 'strait body segment'
			setRotation(segment.obj(), segment.dirToNext());
		}
		//Otherwise this must be a corner body segment
		else {
			//Orient the object as 'corner body segment'
			orientAsCorner(segment.obj(), segment.dirToNext(), segment.dirToPrev());
		}
	}



	//
	//updates the sprite of body segment to match the directions passed as parameters
	//
	public static void orientSegment(GameObject obj, int to, int from) {
		
		//if lastdir - nextdir == 4, this must be a strait body segment
		if(isStrait(to, from)) {
			//Orient object as 'strait body segment'
			setRotation(obj, to);
		}
		//Otherwise this must be a corner body segment
		else {
			//Orient the object as 'corner body segment'
			orientAsCorner(obj, to, from);
		}
	}



	//
	//Sets segment sprite for non-animated segment
	//
	public static void setSegmentSprite(SnakeCircleQueue.BodySegment segment) {
		//if lastdir - nextdir == 4, this must be a strait body segment
		if(isStrait(segment)) {
			
			//Set the sprite to be strait body segment sprite
			segment.obj().GetComponent<SpriteRenderer>().sprite = GeneralSpriteStore.store.SNAKE_STRAIT;
		}
		//Otherwise this must be a corner body segment
		else {
			if(isClockwise(segment)) {
				//Set the sprite to be the corner body segment sprite
				SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.SNAKE_CLOCKWISE);
			}
			else {
				SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.SNAKE_COUNTERCLOCKWISE);
			}
		}
	}



	//
	//Sets egment sprite for animated segment
	//
	public static void setSegmentSpriteAnimated(SnakeCircleQueue.BodySegment segment,
	                                            float percent) {
		if(isStrait(segment)){
			SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeStraitSprite(percent));
		}
		else{
			if(isClockwise(segment))
			    SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeClockwiseSprite(percent));
			else
				SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeCounterClockwiseSprite(percent));
		}
	}



	//
	//Sets egment sprite for animated segment back
	//
	public static void setSegmentSpriteAnimatedB(SnakeCircleQueue.BodySegment segment,
	                                            float percent) {
		if(isStrait(segment)){
			SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeStraitSpriteB(percent));
		}
		else{
			if(isClockwise(segment))
				SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeClockwiseSpriteB(percent));
			else
				SpriteUtil.setSprite(segment.obj(), GeneralSpriteStore.store.getSnakeCounterClockwiseSpriteB(percent));
		}
	}
	
	
	
	//
	//Sets egment sprite for animated segment
	//
	public static void setSegmentSpriteAnimated(GameObject obj, int to, int from, float percent) {
		if(isStrait(to, from)){
			SpriteUtil.setSprite(obj, GeneralSpriteStore.store.getSnakeStraitSprite(percent));
		}
		else{
			if(isClockwise(to, from))
				SpriteUtil.setSprite(obj, GeneralSpriteStore.store.getSnakeClockwiseSprite(percent));
			else
				SpriteUtil.setSprite(obj, GeneralSpriteStore.store.getSnakeCounterClockwiseSprite(percent));
		}
	}



	//
	//Sets orientation of the gameObject to match corner orientation for given direction
	//	Assumes that scale of gameObject is already correct
	//
	public static void orientAsCorner(GameObject obj, int to, int from) {
		//from top to left
		if((from == 6 && to == 4) || (from == 4 && to == 6))
			setRotation(obj, 6);
		else if((from == 4 && to == 2) || (from == 2 && to == 4))
			setRotation(obj, 4);
		else if((from == 2 && to == 0) || (from == 0 && to == 2))
			setRotation(obj, 2);
		else if((from == 0 && to == 6) || (from == 6 && to == 0))
			setRotation(obj, 0);
	}



	//
	//Determines if movement is strait or if is corner
	//
	public static bool isStrait(int to, int from) {
		//for movement to be strait, absolute value of from - to must be 4
		return Mathf.Abs(from - to) == 4;
	}



	//
	//Overloaded version that takes in segment as parameter
	//Determines if movement is strait or if is corner
	//
	public static bool isStrait(SnakeCircleQueue.BodySegment segment) {
		//for movement to be strait, absolute value of from - to must be 4
		return Mathf.Abs(segment.dirToPrev() - segment.dirToNext()) == 4;
	}



	//
	//Determines is movement is clockwise or not based on to-direction and from-direction
	//
	public static bool isClockwise(int to, int from) {
		//If to is greater than from, then on all but one case this movement is clockwise
		if(to > from) {
			//special case when to is greater than from and is counter clockwise
			if(from == 0 && to == 6)
				return false;
			else return true;
		}
		else {//from must be greater than to
			//Special case when from is greater than to and is clockwise
			if(from == 6 && to == 0)
				return true;
			else return false;
		}
	}



	//
	//Overloaded version that takes in segment as parameter
	//Determines is movement is clockwise or not based on to-direction and from-direction
	//
	public static bool isClockwise(SnakeCircleQueue.BodySegment segment) {

		//If to is greater than from, then on all but one case this movement is clockwise
		if(segment.dirToNext() > segment.dirToPrev()) {
			//special case when to is greater than from and is counter clockwise
			if(segment.dirToPrev() == 0 && segment.dirToNext() == 6)
				return false;
			else return true;
		}
		else {//from must be greater than to
			//Special case when from is greater than to and is clockwise
			if(segment.dirToPrev() == 6 && segment.dirToNext() == 0)
				return true;
			else return false;
		}
	}



	//
	//Rotates the gameobject to match the "int" representation of direction
	//The 'multiplier' vector is used to control which axis is rotated around
	//pass in a vector with two 0 values and a 1 value on the axis to be rotated
	//
	public static void rotateAsIntDirection(GameObject obj, int direction, Vector3 multiplier) {
		if(direction == 0)
			obj.transform.Rotate(multiplier * 270);
		else if(direction == 2)
			obj.transform.Rotate(multiplier * 180);
		else if(direction == 4)
			obj.transform.Rotate(multiplier * 90);
		else{
			//No rotation for direction 6
		}
	}
}

