﻿using UnityEngine;
using System.Collections;

//
//Circular queue data structure used to store the gameobjects that are body segments of
//	the snake (not including head and tail of snake).
//Implimented with a double ended doubly linked list
//
public class SnakeCircleQueue {



	BodySegment firstSegment;		//Stores the head
	BodySegment lastSegment;		//Stores the tail
	private static int lastSegmentPrev = 0;	//Special case: stores the "dirToPrev" value for the very last body segment



	//
	//Node class for this linked-list data structure- each node represents a body segment of the snake
	//	The head of the snake is and exception, it is not a node in the SnakeCircleQueue
	//
	public class BodySegment {
		private GameObject gameObj;		//Actual GameObject of the Body Segment
		public BodySegment prev;		//Points to the previous body segment in the list (i.e. towards the tail)
		public BodySegment next;		//Points to the next body segment in the list (i.e. towards the head)
		private int dirToNextVal;		//Value used to determine the actual in game directional relationships between body segments

		//Returns the direction to the previous body segment
		public int dirToPrev() {
			//Check for special case when this is the very last body segment
			if(prev == null)
				return lastSegmentPrev;
			else
			//Otherwise, obtain direction to previous by inverting the previous segment's dirToNext
				return SnakeUtil.invertDirection(prev.dirToNext());
		}

		//Returns the direction to the next body segment
		public int dirToNext() {
			return dirToNextVal;
		}

		//Setter for the dirToNext value
		public void setDirToNext(int setToThis) {
			dirToNextVal = setToThis;
		}

		public BodySegment(GameObject setObject) {
			gameObj = setObject;
		}

		//Accessor for the GameObject stored inside this class (the GameObject is the actual body segment object)
		public GameObject obj() {
			return gameObj;
		}
	}



	//
	//Constructor, must pass in the game objects that are the head and tail of the snake
	//
	public SnakeCircleQueue(){

	}



	//
	//Adds node onto back end of circlequeue
	//
	public void addSegmentLast(GameObject obj, int setDirToNextVal) {

		//Create new body segment and update its dirToNext value
		BodySegment temp = new BodySegment(obj);
		temp.setDirToNext(setDirToNextVal);

		//Check for special case when this is the first segment to be added
		if(firstSegment == null) {
			firstSegment = temp;
			lastSegment = firstSegment;
		}

		//If not special case, add this segment to the end of the list
		else {
			temp.next = lastSegment;
			lastSegment.prev = temp;
			lastSegment = temp;
		}
	}



	//
	//Cycles queue forward (i.e. the last element becomes the first element)
	//
	public void cycleForward(bool addNewSegment) {

		//Do nothing if only one segment exists
		if(firstSegment == lastSegment)
			return;

		//Record current value of lastSegment Prev in case we need to add on a new segment
		int rememberLastPrev = lastSegmentPrev;
		Vector3 rememberLocation = last().obj().transform.position;

		//Update special case value for last segment's prevDirection if not adding new Segment
		//	(remember that after cyccling, last segment's prev direction is what the currently
		//	 second to last segment's prevDirection is right now)
		lastSegmentPrev = lastSegment.next.dirToPrev();

		//Create temp storage of the last segment
		BodySegment temp = lastSegment;

		//Shift lastSegment pointer to the second to last segment
		lastSegment = lastSegment.next;

		//Break backwards link from the second-last segment to the last segment
		lastSegment.prev = null;

		//Break the forward link from the last segment to the second-last segment
		temp.next = null;

		//Link the currently unlinked segment stored in temp ahead of the first segment
		temp.prev = firstSegment;
		firstSegment.next = temp;

		//update the pointer to firstSegment to point at the now-linked segment in temp
		firstSegment = temp;

		if(addNewSegment){
			GameObject newSegment = SnakeUtil.newBodySegment();
			addSegmentLast(newSegment, SnakeUtil.invertDirection(lastSegmentPrev));

			//Set object to correct location
			/*SnakeUtil.setLocation(newSegment, 
			                      		SnakeUtil.getLocFromDir(
											SnakeUtil.getCoords(last().obj()), lastSegmentPrev
								  		)
			                      );*/
			lastSegmentPrev = rememberLastPrev;
			newSegment.transform.position = rememberLocation;
			SnakeUtil.orientSegment(last());
			SnakeUtil.setSegmentSprite(last ());
			//Reset the special value of lastSegmentPrev
		}
	}



	//
	//Orients and sets sprites for all segments
	//
	public void orientAndSetSpriteAll(){
		BodySegment temp = first();
		while (temp != null){
			SnakeUtil.orientSegment(temp);
			SnakeUtil.setSegmentSprite(temp);
			temp = temp.next;
		}
	}



	//
	//Accessor for the first body segment in queue
	//
	public BodySegment first() {
		return firstSegment;
	}



	//
	//Accessor for the last body segment in queue
	//
	public BodySegment last() {
		return lastSegment;
	}



	//
	//Sets the special 'lastSegmentPrev' value
	//
	public void setLastSegPrev(int value){
		lastSegmentPrev = value;
	}
}
