using UnityEngine;
using System.Collections;

//
//A general Utility class for working with sprites
//
public class SpriteUtil {

	//
	//Simplifies the process of setting sprites to a gameObject
	//
	public static void setSprite(GameObject obj, Sprite sprite) {
		obj.GetComponent<SpriteRenderer>().sprite = sprite;
	}

	//
	//Simplifies the process of hiding/showing a sprite GameObject
	//Preconditions: Must GameObject must have a SpriteRenderer component
	//
	public static void setSpriteVisibility(GameObject obj, bool value) {
		obj.GetComponent<SpriteRenderer>().enabled = value;
	}
}

