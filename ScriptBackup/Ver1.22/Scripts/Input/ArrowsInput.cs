﻿using UnityEngine;
using System.Collections;

public class ArrowsInput : MonoBehaviour {


	
	public static bool[] arrowInfo;		//Stores the status of all arrow inputs
										//This array is initialized in PreUpdate's start function



	//
	//Returns true if arrow of specifed direction is in the pressed state (not necisarilly pressed this frame)
	//Direction MUST be between 0 and 7 (if only using the four primary direction, use 0(right) 2(down) 4(left) and 6(up))
	//
	public static bool getArrow(int direction) {
		return arrowInfo[direction];
	}



	//
	//Returns true if arrow of specifed direction was pressed down this frame
	//Direction MUST be between 0 and 7 (if only using the four primary direction, use 0(right) 2(down) 4(left) and 6(up))
	//
	public static bool getArrowDown(int direction) {
		return arrowInfo[direction + 8];
	}



	//
	//Returns true if arrow of specified direction was lifted this frame
	//Direction MUST be between 0 and 7 (if only using the four primary direction, use 0(right) 2(down) 4(left) and 6(up))
	//
	public static bool getArrowUp(int direction) {
		return arrowInfo[direction + 16];
	}



	//
	//Sets all getArrowUp and getArrowDown values to false in preperation for the next frame
	//
	public static void prepForFrame() {
		for(int i = 0; i <= 7; i++) {
			arrowInfo[i + 8] = false;
			arrowInfo[i + 16] = false;
		}
	}
}