﻿using UnityEngine;
using System.Collections;



//
//Stores the world position of the mouse in static var's for easy access
//PreConditions:
//	Must be attached to the main camera object
//
public class Mouse : MonoBehaviour {



	public static Mouse mouse;			//Singleton instance
	Camera gameCamera;					//used for screentoworldpoint calculation
	public static Vector3 worldMouse;	//World location of the mouse (on the X/Y plane)
	public static Vector2 worldMouse2D;	//World Mouse location assuming ussage on 2D plane
	Vector3 mouseRestPos = new Vector3(-500, -500, -500);



	//
	//Awake
	//
	void Awake() {
		mouse = this;
	}



	//
	// Use this for initialization
	//
	void Start () {
		//Get camera for easy access
		gameCamera = GameCamera.gameCameraObject.GetComponent<Camera>();

		//Move mouse to rest position
		transform.position = mouseRestPos;
	}



	//
	// Update is called once per frame - get the current mouse info
	//
	public void UpdateMouse () {
		worldMouse = gameCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		worldMouse2D = new Vector2(worldMouse.x, worldMouse.y);

		//Move mouse object if screen is pressed
		if(Input.GetMouseButton(0))
			transform.position = worldMouse;
		if(Input.GetMouseButtonUp(0))
			transform.position = mouseRestPos;

	}
}