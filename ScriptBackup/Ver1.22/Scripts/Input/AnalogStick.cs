﻿using UnityEngine;
using System.Collections;

//
//Manages the analog stick object and subobjects
//This script should be run before default execution time because it deals with input
//
public class AnalogStick : MonoBehaviour {



	public static AnalogStick analogStick;	//for referencing by other classes	

	private Vector3 homeLocation;	//Location that analog stick returns to when inactive
	private Window window;			//used for checking collision- i.e. collision with window is when analog stick should be activated
	private bool isActive;			//Records if the analog stick is actively tracking the finger's position
	private GameObject CursorPos;	//Visual aid for the cursor position
	private GameObject Trail;		//Visual aid for offset between cursor and center of analog stick
	private GameObject[] arrows;	//Visual aid for direction that analog stick is currently going
	private int curDirection = -1;	//Current direction that this Anaolog stick is pointing (-1 for no direction)
	private bool[] inputInfo;		//Stores arrowPressed (first 8 indeces) arrowDown (next 8) and arrowUp (last 8)


	//
	// Initializes the analog stick - must be called before analog stick becomes functional
	//
	public void init (Vector3 setHome, Window setWindow) {
		analogStick = this;
		window = setWindow;
		homeLocation = setHome;
		inputInfo = new bool[24];
		arrows = new GameObject[8];
		for(int i = 0; i <= 6; i += 2) {
			arrows[i] =  (GameObject) Instantiate(Resources.Load("Arrow"));
			arrows[i].GetComponent<AS_Arrow>().init();
			float offset = transform.localScale.x / 2;
			float scale = transform.localScale.x / 2.4f;
			Vector3 arrowPos = transform.position;
			switch(i) {
			case 0:
			case 4: arrowPos += new Vector3( (i == 0) ? offset : -offset, 0, 0); break;
			case 2:
			case 6: arrowPos += new Vector3( 0, (i == 6) ? offset : -offset, 0); break;
			}
			arrows[i].transform.position = arrowPos;
			arrows[i].transform.localScale = Vector3.one * scale;
			SnakeUtil.rotateAsIntDirection(arrows[i].gameObject, i, Vector3.forward);
			arrows[i].transform.parent = transform;
		}
		setVisibility(false);
	}



	// Update is called once per frame
	void Update () {

		//Clear the input to create blank slate for this frame
		clearInput();

		//Check if finger was pressed or lifted
		if(Input.GetMouseButtonDown(0)) {
			if(window.checkCollision(Mouse.worldMouse)) {
				transform.position = new Vector3(Mouse.worldMouse.x, Mouse.worldMouse.y, 0);
				isActive = true;
				CursorPos = (GameObject) Instantiate(Resources.Load("AS_CursorPos"));
				CursorPos.transform.localScale = Vector3.one * transform.localScale.x / 3;
				Trail = (GameObject) Instantiate(Resources.Load ("AS_Trail"));
				setVisibility(true);
			}
		}
		if(Input.GetMouseButtonUp(0)) {
			isActive = false;
			tryDirUpdate(-1);
			Destroy(CursorPos);
			Destroy(Trail);
			transform.position = homeLocation;
			setVisibility(false);
		}

		//If active, update the AnalogStick
		if(isActive){

			//Update CursorPos object
			CursorPos.transform.position = Mouse.worldMouse;

			//Find offset vector, angle, and magnitude
			Vector3 offset = CursorPos.transform.position - transform.position;
			Vector2 offset2 = new Vector2(offset.x, offset.y);
			float angle = Mathf.Atan2(offset.y, offset.x);
			angle = angle * 180f/Mathf.PI;
			if(angle < 0)
				angle += 360f;
			float magnitude = Mathf.Sqrt(Vector2.Dot(offset2, offset2));

			//Update the lineRenderer
			Trail.GetComponent<LineRenderer2D>().render(transform.position, offset, angle, magnitude, transform.localScale.x / 5);

			//Determine new direction
			int newDirection;
			newDirection = convertToDirection(angle);

			//See if direction needs to be updated
			tryDirUpdate(newDirection);
		}
	}



	//
	//Accessor for current direction
	//
	public int getDirection() {
		return curDirection;
	}



	//
	//Accessor for ArrowPressed value
	//
	public bool arrowPressed(int direction){
		return inputInfo[direction];
	}



	//
	//Returns direction (as int) of any arrow that is being held down
	//returns -1 if no arrow is being held down
	//
	public int whichArrowPressed() {
		for(int i = 0; i <= 7; i++)
			if (inputInfo[i] == true)
				return i;
		return -1;
	}



	//
	//Accessor for ArrowDown value
	//
	public bool arrowDown(int direction) {
		return inputInfo[8 + direction];
	}



	//
	//Returns direction (as int) of any arrow that was "pressed"
	//returns -1 if no arrow was "pressed"
	//
	public int whichArrowDown() {
		for(int i = 0; i <= 7; i++)
			if (inputInfo[8 + i] == true)
				return i;
		return -1;
	}



	//
	//Accessor for ArrowUp value
	//
	public bool arrowUp(int direction) {
		return inputInfo[16 + direction];
	}



	//
	//Returns direction (0, 2, 4, or 6) from an angle in degrees
	//
	private int convertToDirection(float angle) {
		//Find direction that analog stick is pointing
		int direction;
		if(angle <= 45 || angle > 315)
			direction = 0;
		else if(angle <= 135 && angle > 45)
			direction = 6;
		else if(angle <= 225 && angle > 135)
			direction = 4;
		else direction = 2;
		return direction;
	}



	//
	//Tries to update arrow direction (update will only occur if direction is different than curent direction
	//
	private void tryDirUpdate(int newDirection) {
		if(newDirection != curDirection){
			if(newDirection != -1) {
				//change arrow sprite and record "arrowDown" input action
				arrows[newDirection].GetComponent<AS_Arrow>().setActive(true);
				inputInfo[8 + newDirection] = true;
				inputInfo[newDirection] = true;
			}
			if(curDirection != -1) {
				//change arrow sprite and record "arrowUp" input action
				arrows[curDirection].GetComponent<AS_Arrow>().setActive(false);
				inputInfo[16 + curDirection] = true;
				inputInfo[curDirection] = false;
			}
			curDirection = newDirection;
		}
	}



	//
	//Clears the input- sets all arrowDown and arrowUp values to false
	//
	private void clearInput() {
		for(int i = 8; i < 24; i++) {
			inputInfo[i] = false;
		}
	}



	//
	//sets visiblity of all elements of the analog stick
	//
	public void setVisibility(bool value){
		for(int i = 0; i <= 6; i += 2) {
			SpriteUtil.setSpriteVisibility(arrows[i], value);
		}
	}
}
