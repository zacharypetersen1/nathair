﻿using UnityEngine;
using System.Collections;

public class AS_Arrow : MonoBehaviour {



	public Sprite idle, active;
	SpriteRenderer spriteRenderer;



	//
	//Sets the direction of this arrow
	//
	public void init() {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		setActive(false);
	}



	//
	//Changes if this arrow is active or not (pass in true for active, false for innactive)
	//
	public void setActive(bool value) {
		if(value)
			spriteRenderer.sprite = active;
		else
			spriteRenderer.sprite = idle;
	}
}
