from PIL import Image
from sys import argv



#check if arguments were entered correctly
if len(argv) != 4:
   print "Incorrect amount of arguments"
   print "usage: python ColorBlindSimulator.py <worldName> <worldNumber> <configFileName>"
   exit(1)
   
#unpack arguments
scriptName, worldName, worldNumber, configFileName = argv

#Try  to load config
try:
   configFile = open(configFileName)
except IOError:
   print "Template file named: '%s' could not be found" % configFileName
   exit(2)



RGBValue = [[], [], [], [], []]

#Read config file to get info
for j in range(0, 5):
   line = configFile.readline()

   #Split line into array of words
   elements = line.split()

   #store values from line
   try:
      readRed, readGreen, readBlue = elements
   except ValueError:
      print "Incorrect usage within config file:"
      print "Incorrect line: %s" % line
      print "Correct Usage: <redValue> <greenValue> <blueValue>"
      exit(3)
   red = int(readRed)
   green = int(readGreen)
   blue = int(readBlue)
   RGBValue[j] = [red, green, blue]



for i in range(1, 13):
   
   inputName = "1-" + str(i) + "_Unselected.png"

   print "working on: " + inputName

   #Try to load input image
   try:
      inputImage = Image.open(inputName)
   except IOError:
      print "Input image named: '%s' could not be found" % inputName
      exit(2)

   #make a copy of image
   outputImage = inputImage.copy()

   #read all pixels
   for w in range(0, outputImage.size[0]):
      for h in range(0, outputImage.size[1]):
         tempR, tempG, tempB, tempA = outputImage.getpixel((w,h))
         #alpha
         if(tempA == 0):
            #outputImage.putpixel((w,h), (0, 0, 0, 0))
            continue
         #border
         elif(tempR == 109 and tempG == 109 and tempB == 154):
            outputImage.putpixel((w,h), (RGBValue[0][0], RGBValue[0][1], RGBValue[0][2], 255))
         #border shadow
         elif(tempR == 71 and tempG == 71 and tempB == 101):
            outputImage.putpixel((w,h), (RGBValue[1][0], RGBValue[1][1], RGBValue[1][2], 255))
         #inner fill
         elif(tempR == 87 and tempG == 87 and tempB == 123):
            outputImage.putpixel((w,h), (RGBValue[2][0], RGBValue[2][1], RGBValue[2][2], 255))
         #text
         elif(tempR == 146 and tempG == 146 and tempB == 255):
            outputImage.putpixel((w,h), (RGBValue[3][0], RGBValue[3][1], RGBValue[3][2], 255))
         #text shadow
         elif(tempR == 54 and tempG == 54 and tempB == 78):
            outputImage.putpixel((w,h), (RGBValue[4][0], RGBValue[4][1], RGBValue[4][2], 255))
   
   #save out image
   outputImage.save("../Selected/" + str(i) + "_Selected.png")